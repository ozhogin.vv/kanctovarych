import { createStore } from 'vuex';
import { productsModule } from './productsModule';
import { commentsModule } from './commentsModule';

export default createStore({
    modules: {
        products: productsModule,
        comments: commentsModule,
    }
})
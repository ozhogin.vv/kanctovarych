import axios from 'axios';


export const commentsModule = {
    state: () => ({
        comments: []
    }),
    getters: {
        COMMENTS: state => {
            return state.comments;
        },
    },
    mutations: {
        SET_COMMENTS(state, comments) {
            state.comments = comments
        },
        ADD_COMMENT(state, new_comment) {
            state.comments.push(new_comment)
        },
        DELETE_COMMENT(state, comment_id) {
            let comment = state.comments.filter((comment) => {
                return comment.id == comment_id;
            })[0];
            state.comments.splice(comment, 1)
        }
    },
    actions: {
        async FETCH_COMMENTS({commit}, product_id) {
            try {
                const response = await axios.get(`http://127.0.0.1:8000/api/v1/products/${product_id}/comments/`);
                commit('SET_COMMENTS', response.data);
            } catch (e) {
                alert('Ошибка')
            }
        },
        async CREATE_COMMENT({commit}, [product_id, comment]) {
            comment.id = Date.now()
            await axios.post(`http://127.0.0.1:8000/api/v1/products/${product_id}/comment/`, JSON.stringify(comment), {
                headers: {
                  'Content-Type': 'application/json'
                }
            })
            let new_comment = {}
            Object.assign(new_comment, comment)
            comment.id = '';
            comment.title = '';
            comment.text = '';
            comment.user = '';
            commit('ADD_COMMENT', new_comment)
        },
        async DELETE_COMMENT({commit}, comment_id) {
            await axios.delete(`http://127.0.0.1:8000/api/v1/comments/${comment_id}/`);
            commit('DELETE_COMMENT', comment_id)
        },
    },
    namespaced: true
}
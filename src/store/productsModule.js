import axios from 'axios';

export const productsModule = {
    state: () => ({
        products: [],
        product: Object,
    }),
    getters: {
        PRODUCTS: state => {
            return state.products;
        },
        PRODUCT: state => {
            return state.product;
        },
    },
    mutations: {
        SET_PRODUCTS(state, products) {
            state.products = products
        },
        SET_PRODUCT(state, product) {
            state.product = product
        },
    },
    actions: {
        async FETCH_PRODUCTS({commit}) {
            try {
                const response = await axios.get('http://127.0.0.1:8000/api/v1/products/');
                commit('SET_PRODUCTS', response.data);
            } catch (e) {
                alert('Ошибка')
            }
        },
        async GET_PRODUCT({getters, commit, dispatch}, productURL) {
            if (getters.PRODUCTS.length == 0) {
                await dispatch('FETCH_PRODUCTS')
            }
            commit('SET_PRODUCT', getters.PRODUCTS.filter((product) => {
                    return product.url == productURL;
                })[0]
            ) 
        }
    },
    namespaced: true
}
import MainPage from '@/pages/MainPage';
import ProductPage from '@/pages/ProductPage';
import {createRouter, createWebHistory} from 'vue-router';

const routes = [
    { 
        path: '/', 
        component: MainPage 
    },
    { 
        path: '/catalog/:producturl', 
        name: 'catalog',
        component: ProductPage,
     }
]

const router = createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
})

export default router;